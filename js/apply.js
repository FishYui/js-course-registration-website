document.getElementsByClassName('btn-save')[0].getElementsByTagName('button')[0].onclick = function (e) {
    let JsonStr = {
        'name': document.getElementsByName('name')[0].value,
        'phone': document.getElementsByName('phone')[0].value,
        'email': document.getElementsByName('email')[0].value,
        'service': document.getElementsByName('service')[0].value,
        'profession': document.getElementsByName('profession')[0].value,
        'remark': document.getElementsByName('remark')[0].value
    }
    let err = 0;

    if (!JsonStr['name']) {
        document.getElementsByClassName('message')[0].textContent = '請輸入姓名';
        err++;
    } else {
        document.getElementsByClassName('message')[0].textContent = '';
    }

    if (!JsonStr['phone']) {
        document.getElementsByClassName('message')[1].textContent = '請輸入電話';
        err++;
    } else {
        document.getElementsByClassName('message')[1].textContent = '';
    }

    if (!JsonStr['email']) {
        document.getElementsByClassName('message')[2].textContent = '請輸入Email';
        err++;
    } else {
        document.getElementsByClassName('message')[2].textContent = '';
    }

    if (!JsonStr['service']) {
        document.getElementsByClassName('message')[3].textContent = '請輸入服務機溝/單位';
        err++;
    } else {
        document.getElementsByClassName('message')[3].textContent = '';
    }

    if (!JsonStr['profession']) {
        document.getElementsByClassName('message')[4].textContent = '請輸入職稱';
        err++;
    } else {
        document.getElementsByClassName('message')[4].textContent = '';
    }

    if (err == 0) {
        fetch('/apply', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(JsonStr)
        })
            .then(function checkStatus(response) {
                if (response.status >= 200 && response.status < 300) {
                    return response.json();
                } else {
                    var error = new Error(response.statusText)
                    error.response = response;
                    throw error;
                }
            })
            .then(function (data) {
                //完成
                location.href = '/apply/success';
            }).catch(function (error) {
                //錯誤
                location.href = '/apply/fail';
            })
    }
}

document.getElementsByClassName('btn-back')[0].getElementsByTagName('button')[0].onclick = function (e) {
    history.back();
}