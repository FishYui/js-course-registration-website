醫學及資訊跨域人才培育
免費線上課程:網頁前端應用程式(JS 程式)快速入門

課程目標: 學習 HTML 網頁表單及其處理JavaScript程式，並後端標準化 FHIR API 整合應用。建構資料圖表呈現、網路報名、網頁訂單、醫護表單、醫學影像報告等各式應用。

課程內容
Web 技術簡介: 概述網頁前後端及課程應用 蕭
VS code 安裝及使用 子億
html 簡介 1 - 基本標籤  陳
html 簡介 2 - HTML Form 輸入語法 陳
Javascript 簡介: 寫在哪裡、alert、documentGetElementById 巫
Javascript 基礎語法:型別、變數與運算子 巫
if 條件判斷  魯
for 迴圈   魯
Javascript 函式  余
JS 事件  余
JS array 楊
JS JSON 物件 楊