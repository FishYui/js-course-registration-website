const express = require('express');

const app = express();
//const PORT = process.env.PORT || 3389;
const PORT = process.env.PORT || 5000;

app.use('/css', express.static('css'));
app.use('/js', express.static('js'));

app.use('/', require('./router/index'));

app.listen(PORT, console.log(`Server started on port ${PORT}`));