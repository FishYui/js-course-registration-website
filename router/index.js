const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');

const router = express.Router();
const jsonParser = bodyParser.json();

router.get('/', (req, res) => {
    res.sendFile(`${process.cwd()}/view/index.html`);
});

router.get('/apply', (req, res) => {
    res.sendFile(`${process.cwd()}/view/apply.html`);
});

router.post('/apply', jsonParser, (req, res) => {
    if (!req) return res.sendStatus(400);
    fs.appendFile(`${process.cwd()}/file/apply.json`, `${JSON.stringify(req.body)}\n`, (err) => {
        if (!err) {
            console.log('寫入成功!');
            res.json(true);
        }
        else
            console.log(err);
    })
});

router.get('/apply/success', (req, res) => {
    res.sendFile(`${process.cwd()}/view/saveSuccessful.html`);
});

router.get('/apply/fail', (req, res) => {
    res.sendFile(`${process.cwd()}/view/saveFail.html`);
});

module.exports = router;